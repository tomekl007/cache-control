package com.baeldung.cachecontrol;

import com.baeldung.cachecontrol.model.TimestampDto;
import com.baeldung.cachecontrol.model.UserDto;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.CacheControl;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.concurrent.TimeUnit;

import static org.slf4j.LoggerFactory.getLogger;

@Controller
public class ResourceEndpoint {
    private static final Logger log = getLogger(ResourceEndpoint.class);

    @GetMapping(value = "/default/users/{name}")
    public ResponseEntity<UserDto> getUserWithDefaultCaching(@PathVariable String name) {
        return ResponseEntity.ok(new UserDto(name));
    }

    @GetMapping("/users/{name}")
    public ResponseEntity<UserDto> getUser(@PathVariable String name) {
        return ResponseEntity
                .ok()
                .cacheControl(CacheControl.maxAge(60, TimeUnit.SECONDS))
                .body(new UserDto(name));
    }

    @GetMapping("/timestamp")
    public ResponseEntity<TimestampDto> getServerTimestamp() {
        return ResponseEntity
                .ok()
                .cacheControl(CacheControl.noStore())
                .body(new TimestampDto(LocalDateTime
                        .now()
                        .toInstant(ZoneOffset.UTC)
                        .toEpochMilli()));
    }

    @GetMapping("/private/users/{name}")
    public ResponseEntity<UserDto> getUserNotCached(@PathVariable String name) {
        return ResponseEntity
                .ok()
                .cacheControl(CacheControl
                        .noStore()
                        .mustRevalidate()
                )
                .body(new UserDto(name));
    }

    @Value("classpath:home.html")
    private Resource homeHtml;


    @GetMapping("/home")
    public ResponseEntity<String> getHomeHtml() throws IOException {
        log.info("Get for resource home");
        try (InputStream is = homeHtml.getInputStream()) {
            String content = IOUtils.toString(is);
            return ResponseEntity
                    .ok()
                    .contentType(MediaType.TEXT_HTML)
                    .cacheControl(
                            CacheControl
                                    .maxAge(1, TimeUnit.DAYS)
                                    .cachePublic()
                    )
                    .body(content);
        }
    }
}
